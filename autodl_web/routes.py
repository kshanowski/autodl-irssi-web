import os
import secrets
from PIL import Image
from flask import render_template, url_for, redirect, request
from autodl_web import app, db
from autodl_web.forms import SelectionForm, SettingsForm, GlobalSettingsForm, SearchForm
from autodl_web.models import Entry, Settings, GlobalSettings
from autodl_web.generate_cfg import cfg_dump
from werkzeug import secure_filename
import requests
from io import BytesIO
import tmdbsimple as tmdb
import re
from collections import Counter

home=os.getenv("HOME")

@app.route("/", methods=['GET', 'POST'])
def home(): # home page; lists all current entries
    entries = Entry.query.all()
    check_status = check_download_status(entries)
    return render_template('home.html', title='current db', entries=entries, check_status=check_status)

def check_download_status(entries):
    # checks if entry in db was downloaded by autodl
    # it uses DownloadHistory.txt that autodl generates
    # using regex; if entry title (saved as alphanumeric only) is present in history file
    # if yes, appends it to a list, which is further looped in template file
    # to diplay if entry was downloaded
    log_file_path = home+'/.autodl/DownloadHistory.txt'
    downloaded = []
    for entry in entries:
        p=re.compile(entry.shows, re.IGNORECASE)
        with open(log_file_path, "r") as file:
            for line in file:
                if p.search(line):
                    downloaded.append(entry.shows)
    return Counter(downloaded)
        

def save_picture(form_picture):
    # if poster is not present in tmdb, use the default, local, 'no image' image;
    # if it is, use provided url to download, resize and save poster locally
    if form_picture == '':
        poster = 'static/images/default.jpg'
        return poster 
    else:
        path = 'https://image.tmdb.org/t/p/w200/' + form_picture
        random_hex = secrets.token_hex(8)
        _, f_ext = os.path.splitext(path)
        picture_fn = random_hex + f_ext
        picture_path = os.path.join(app.root_path, 'static/images', picture_fn)
        output_size = (150, 150)
        response = requests.get(path)
        i = Image.open(BytesIO(response.content))
        i.thumbnail(output_size)
        i.save(picture_path)
        poster = 'static/images/' + str(picture_fn)
        return poster 

def tmdb_find(searching_for, show_type):
    tmdb.API_KEY = '' 
    search = tmdb.Search()
    if show_type == 'movie':
        response = search.movie(query=searching_for)
    elif show_type == 'tv':
        response = search.tv(query=searching_for)
    results = search.results
    #filter search results for titles w/o date of release (witch can happen in tmdb)
    #and raises an error during searching
    for s in results:
        if 'release_date' in s or 'first_air_date' in s: # check if key is present in dict
            pass # if it is, do nothing
        else:
            s['release_date'] = '' # add an empty key in case it is not present
            s['first_air_date'] = ''
    return results 

form_value = ''
show_type = ''
@app.route("/search", methods=['GET', 'POST'])
def search_tmdb():
    form=SearchForm()
    if form.validate_on_submit():
        global form_value
        global show_type
        form_value = form.search.data
        show_type = form.show_type.data
        return redirect(url_for('search_results'))
    return render_template('search.html', title='search', form=form)

@app.route("/search_results", methods=['GET', 'POST'])
def search_results():
    form=SearchForm()
    global form_value
    global show_type
    search_results_loop = tmdb_find(form_value, show_type)
        
    return render_template('search_results.html', title='search results', show_type=show_type, search_results_loop=search_results_loop)

@app.route("/add_new_entry", methods=['GET', 'POST'])
def add_new_entry():
    global show_type
    form=SelectionForm()
    # get info about title and release date from dynamic url (or whatever it is called)
    if request.method == 'GET':
        # get only alphanumeric from titles and put into form
        # thats the best way I could do it :(
        form.shows.data=re.findall('(\w+)', request.args['shows'])
        form.shows.data=" ".join(form.shows.data)
        form.years.data=request.args['years']

    if form.validate_on_submit():
        entry = Entry(show_type=show_type,match_sites=form.match_sites.data, max_size=form.max_size.data, shows=form.shows.data, sources=form.sources.data, years=form.years.data,
                resolutions=form.resolutions.data, max_downloads=form.max_downloads.data,
                max_downloads_per=form.max_downloads_per.data,seasons=form.seasons.data,
                smartepisode=form.smartepisode.data,image_file=save_picture(request.args['image_file']))
        db.session.add(entry) # add to the db
        db.session.commit() # commit
        cfg_dump() # generate config file for irssi-autodl
        return redirect(url_for('home'))
    return render_template('add_new_entry.html', title='autodl web', form=form, show_type=show_type, legend = 'Add new entry')

@app.route("/entry/<int:entry_id>")
def entry(entry_id):
    entry = Entry.query.get_or_404(entry_id)
    return render_template('entry.html', title=entry.shows, entry=entry)

@app.route("/entry/<int:entry_id>/delete", methods=['POST'])
def delete_entry(entry_id):
    entry = Entry.query.get_or_404(entry_id)
    os.remove(app.root_path +"/"+ entry.image_file) # delete locally saved poster
    db.session.delete(entry)
    db.session.commit()
    cfg_dump()
    return redirect(url_for('home'))

@app.route("/settings", methods=['GET', 'POST'])
def settings():
    settings = Settings.query.all()
    return render_template('settings.html', title='settings', settings=settings)

@app.route("/settings_add_new", methods=['GET', 'POST'])
def settings_add_new():
    form=SettingsForm()
    if form.validate_on_submit():

        settings = Settings(server_name=form.server_name.data,server_enabled=form.server_enabled.data,server_port=form.server_port.data,server_ssl=form.server_ssl.data,server_nick=form.server_nick.data,server_bnc=form.server_bnc.data,channel_name=form.channel_name.data,tracker_type=form.tracker_type.data,tracker_enabled=form.tracker_enabled.data,tracker_force_ssl=form.tracker_force_ssl.data,tracker_upload_delay_secs=form.tracker_upload_delay_secs.data,tracker_rsskey=form.tracker_rsskey.data)

        db.session.add(settings)
        db.session.commit()
        cfg_dump()
        return redirect(url_for('settings'))
    return render_template('settings_add_new.html', title='Add new settings',
            form=form, legend='Add new tracker')

@app.route("/setting/<int:setting_id>")
def setting(setting_id):
    setting = Settings.query.get_or_404(setting_id)
    return render_template('settings.html', title='tracker settings',setting=setting)

@app.route("/settings/<int:setting_id>/update", methods=['GET', 'POST'])
def edit_setting(setting_id):
    setting = Settings.query.get_or_404(setting_id)
    form = SettingsForm()
    if form.validate_on_submit():
        setting.server_name=form.server_name.data
        setting.server_enabled=form.server_enabled.data
        setting.server_port=form.server_port.data
        setting.server_ssl=form.server_ssl.data
        setting.server_nick=form.server_nick.data
        setting.server_bnc=form.server_bnc.data
        setting.channel_name=form.channel_name.data
        setting.tracker_type=form.tracker_type.data
        setting.tracker_enabled=form.tracker_enabled.data
        setting.tracker_force_ssl=form.tracker_force_ssl.data
        setting.tracker_upload_delay_secs=form.tracker_upload_delay_secs.data
        setting.tracker_rsskey=form.tracker_rsskey.data
        db.session.commit()
        cfg_dump()
        return redirect(url_for('settings', setting_id=setting.id))
    else:# request.method == 'GET':
        form.server_name.data=setting.server_name
        form.server_enabled.data=setting.server_enabled
        form.server_port.data=setting.server_port
        form.server_ssl.data=setting.server_ssl
        form.server_nick.data=setting.server_nick
        form.server_bnc.data=setting.server_bnc
        form.channel_name.data=setting.channel_name
        form.tracker_type.data=setting.tracker_type
        form.tracker_enabled.data=setting.tracker_enabled
        form.tracker_force_ssl.data=setting.tracker_force_ssl
        form.tracker_upload_delay_secs.data=setting.tracker_upload_delay_secs
        form.tracker_rsskey.data=setting.tracker_rsskey
    return render_template('settings_add_new.html', title='Edit tracker settings',form=form, legend='Edit tracker')

@app.route("/settings/<int:setting_id>/delete", methods=['POST'])
def delete_setting(setting_id):
    setting = Settings.query.get_or_404(setting_id)
    db.session.delete(setting)
    db.session.commit()
    cfg_dump()
    return redirect(url_for('settings'))

# global settings
@app.route("/settings_global", methods=['GET', 'POST'])
def settings_global():
    form = GlobalSettingsForm()

    if form.validate_on_submit():
        global_settings = GlobalSettings(upload_type=form.upload_type.data,upload_watch_dir=form.upload_watch_dir.data,auto_connect=form.auto_connect.data,user_name=form.user_name.data, real_name=form.real_name.data,close_nickserv = form.close_nickserv.data)

        db.session.add(global_settings)
        db.session.commit()
        cfg_dump()
        return redirect(url_for('home'))
    return render_template('settings_global.html', title='settings', form=form)
