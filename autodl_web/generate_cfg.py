from autodl_web import db
from autodl_web.models import Entry, Settings, GlobalSettings
from os import getenv

home=getenv("HOME")

def cfg_dump():
    file = open(home+'/.autodl/autodl.cfg', 'w')

    settings_global = GlobalSettings.query.all()
    for setting in settings_global:
        file.write("[options]" + "\n")
        file.write("upload-type = " + str(setting.upload_type)+ "\n")
        file.write("upload-watch-dir = " + str(setting.upload_watch_dir)+ "\n")
        file.write("[irc]" + "\n")
        file.write("auto-connect = " + str(setting.auto_connect)+ "\n")
        file.write("user-name = " + str(setting.user_name)+ "\n")
        file.write("real-name = " + str(setting.real_name)+ "\n")
        file.write("close-nickserv = " + str(setting.close_nickserv)+ "\n")


    settings = Settings.query.all()
    for setting in settings:
        file.write("[server " + str(setting.server_name) + "]"+ "\n")
        file.write("enabled = " + str(setting.server_enabled)+ "\n")
        file.write("port = " + str(setting.server_port)+ "\n")
        file.write("ssl = " + str(setting.server_ssl)+ "\n")
        file.write("nick = " + str(setting.server_nick)+ "\n")
        file.write("bnc = " + str(setting.server_bnc)+ "\n")
        file.write("[channel " + str(setting.server_name) + "]"+ "\n")
        file.write("name = " + str(setting.channel_name)+ "\n")
        file.write("[tracker " + str(setting.tracker_type) + "]"+ "\n")
        file.write("enabled = " +str(setting.tracker_enabled)+ "\n")
        file.write("force-ssl = " + str(setting.tracker_force_ssl)+ "\n")
        file.write("upload-delay-secs = " + str(setting.tracker_upload_delay_secs)+ "\n")
        file.write("rsskey = " + str(setting.tracker_rsskey)+ "\n")
        file.write("\n")

    dbase = Entry.query.all()
    for record in dbase:
        file.write("[filter " + str(record.shows) + "_" + str(record.show_type) + "]" + "\n")
        file.write("match-sites = " + str(record.match_sites) + "\n")
        file.write("max-size = " + str(record.max_size) + "\n")
        file.write("shows = " + str(record.shows) + "\n")
        file.write("sources = " + str(record.sources) + "\n")
        file.write("resolutions = " + str(record.resolutions) + "\n")
        file.write("max-downloads = " + str(record.max_downloads) + "\n")
        file.write("max-downloads-per = " + str(record.max_downloads_per) + "\n")
        if record.show_type == 'tv':
            file.write("seasons = " + str(record.seasons) + "\n")
            file.write("smart-episode = " + str(record.smartepisode) + "\n")
        elif record.show_type == 'movie':
            file.write("years = " + str(record.years) + "\n")
        file.write("\n")
    file.close()
