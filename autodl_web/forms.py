from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, SubmitField, BooleanField, SelectField
from wtforms.validators import DataRequired, Length 
from autodl_web.models import Entry, Settings

class SearchForm(FlaskForm):
    search = StringField('')
    show_type = SelectField('show type', choices=[('movie', 'movie'), ('tv', 'tv')])

    submit = SubmitField('Search')

class SelectionForm(FlaskForm):
    match_sites = StringField('match_sites', default='tl')
    max_size = StringField('max_size', default='20GB')
    shows = StringField('shows', validators=[DataRequired()])
    sources = StringField('sources', default='BluRay, WEB-DL, WEB')
    years = StringField('years')
    resolutions = StringField('resolutions', default='1080p')
    max_downloads = StringField('max_downloads', default='1')
    max_downloads_per = SelectField('max_downloads_per', choices=[('month', 'month'), ('week', 'week')], default='month')
    seasons = StringField('seasons', default='')
    smartepisode = SelectField('smartepisode', choices=[('False', 'False'), ('True', 'True')], default='True')
    image_file = StringField('image_file')                 
    
    submit = SubmitField('Save')


class SettingsForm(FlaskForm):
    # [server]
    server_name = StringField('server_name')
    server_enabled = SelectField('server_enabled', choices=[('False', 'False'), ('True', 'True')])
    server_port = StringField('server_port')
    server_ssl = SelectField('server_ssl', choices=[('False', 'False'), ('True', 'True')])
    server_nick = StringField('server_nick')
    server_bnc = SelectField('server_bnc', choices=[('False', 'False'), ('True', 'True')])
    # [channel]
    channel_name = StringField('channel_name')
    # [tracker]
    tracker_type = StringField('tracker_type')
    tracker_enabled = SelectField('tracker_enabled', choices=[('False', 'False'), ('True', 'True')])
    tracker_force_ssl = SelectField('tracker_force_ssl', choices=[('False', 'False'), ('True', 'True')])
    tracker_upload_delay_secs = StringField('tracker_upload_delay_secs')
    tracker_rsskey = StringField('tracker_rsskey')


    submit = SubmitField('Save')
    
class GlobalSettingsForm(FlaskForm):
    # [options]
    upload_type = SelectField('upload_type', choices=[('watchdir', 'watchdir')])
    upload_watch_dir = StringField('upload_watch_dir')
    # [irc]
    auto_connect = SelectField('auto_connect', choices=[('False', 'False'), ('True', 'True')])
    user_name = StringField('user_name')
    real_name = StringField('real_name')
    close_nickserv = SelectField('close_nickserv', choices=[('False', 'False'), ('True', 'True')])


    submit = SubmitField('Save')


