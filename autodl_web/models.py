from autodl_web import db

class GlobalSettings(db.Model): 
    id = db.Column(db.Integer, primary_key=True)
    # [options]
    upload_type = db.Column(db.String(), nullable=False)
    upload_watch_dir = db.Column(db.String(), nullable=False)
    # [irc]
    auto_connect = db.Column(db.String(), nullable=False)
    user_name = db.Column(db.String(), nullable=True)
    real_name = db.Column(db.String(), nullable=True)
    close_nickserv = db.Column(db.String(), nullable=False)

class Settings(db.Model): 
    id = db.Column(db.Integer, primary_key=True)
    # [server]
    server_name = db.Column(db.String(), nullable=False)
    server_enabled = db.Column(db.String(), nullable=False)
    server_port = db.Column(db.String(), nullable=False)
    server_ssl = db.Column(db.String(), nullable=False)
    server_nick = db.Column(db.String(), nullable=False)
    server_bnc = db.Column(db.String(), nullable=False)
    # [channel]
    channel_name = db.Column(db.String(), nullable=False)
    # [tracker]
    tracker_type = db.Column(db.String(), nullable=False)
    tracker_enabled =db.Column(db.String(), nullable=False)
    tracker_force_ssl =db.Column(db.String(), nullable=False)
    tracker_upload_delay_secs = db.Column(db.String(), nullable=False)
    tracker_rsskey = db.Column(db.String(), nullable=False)

class Entry(db.Model): 
    id = db.Column(db.Integer, primary_key=True)
    show_type = db.Column(db.String(), nullable=False) # movie, tv, music, game, book
    match_sites = db.Column(db.String(), nullable=False)
    max_size = db.Column(db.String(), nullable=False)
    shows =  db.Column(db.String(), nullable=False)
    sources = db.Column(db.String(), nullable=False)
    years = db.Column(db.String(), nullable=True)
    resolutions = db.Column(db.String(), nullable=False)
    max_downloads =db.Column(db.String(), nullable=True)
    max_downloads_per =db.Column(db.String(), nullable=False)
    seasons =db.Column(db.String(), nullable=True)
    smartepisode =db.Column(db.String(), nullable=True)
    image_file = db.Column(db.String(20), default='default.jpg')


    
                           
